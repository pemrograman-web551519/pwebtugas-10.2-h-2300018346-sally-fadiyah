<?php
$nilai = 50;
$pesan = "";
$status = "";

if ($nilai >= 60) {
    $pesan = "Selamat! Nilai Anda $nilai. Anda LULUS 🎉";
    $status = "pass";
} else {
    $pesan = "Maaf, Nilai Anda $nilai. Anda GAGAL 😢";
    $status = "fail";
}
?>
<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hasil Ujian</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Montserrat', sans-serif;
            background: linear-gradient(to right, #74ebd5, #acb6e5);
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: #333;
            overflow: hidden; /* Prevents scrollbars from showing */
        }
        .container {
            background-color: #fff;
            padding: 40px;
            border-radius: 15px;
            text-align: center;
            box-shadow: 0 10px 30px rgba(0, 0, 0, 0.15);
            max-width: 400px;
            width: 100%;
            transition: transform 0.3s, box-shadow 0.3s;
            position: relative; /* For rain emoji positioning */
        }
        .container:hover {
            transform: translateY(-10px);
            box-shadow: 0 20px 40px rgba(0, 0, 0, 0.2);
        }
        h1 {
            margin-bottom: 20px;
            font-size: 2.2em;
            color: #333;
        }
        .message {
            font-size: 1.2em;
            margin: 20px 0;
        }
        .pass {
            color: #27ae60;
        }
        .fail {
            color: #c0392b;
        }
        .retry-button {
            background-color: #3498db;
            color: #fff;
            padding: 12px 25px;
            border: none;
            border-radius: 25px;
            font-size: 1em;
            cursor: pointer;
            transition: all 0.3s ease;
            outline: none;
            display: inline-block;
            margin-top: 20px;
        }
        .retry-button:hover {
            background-color: #2980b9;
            transform: scale(1.05);
        }
        @keyframes fadeIn {
            from {
                opacity: 0;
                transform: translateY(20px);
            }
            to {
                opacity: 1;
                transform: translateY(0);
            }
        }
        .container {
            animation: fadeIn 1s ease-in-out;
        }
        .rain {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            display: flex;
            flex-wrap: wrap;
            width: 100%;
            height: 100%;
            pointer-events: none; /* Allows interaction with buttons underneath */
        }
        .rain span {
            font-size: 2em;
            animation: drop 1s infinite;
            opacity: 0;
        }
        @keyframes drop {
            0% {
                opacity: 0;
                transform: translateY(-100px);
            }
            50% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                transform: translateY(100px);
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Hasil Ujian Anda</h1>
        <p class="message <?php echo $status; ?>">
            <?php echo $pesan; ?>
        </p>
        <button class="retry-button" onclick="window.location.reload();">Cek Nilai Lagi</button>
        <?php if ($status == "fail"): ?>
            <div class="rain">
                <?php for ($i = 0; $i < 50; $i++): ?>
                    <span style="animation-delay: <?php echo rand(0, 1000) / 1000; ?>s;">😢</span>
                <?php endfor; ?>
            </div>
        <?php endif; ?>
    </div>
</body>
</html>
